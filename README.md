## Guven

An ultimate authentication system.


#### Description

We need an authentication service, which will be placed in our services to play as a part of our microservice solution.

Guven needs to be able to:
- Create new users and save their data
- Authenticate existing users and return a token
- Verify tokens

You need to write Feature tests to make sure the functionality works fine.
